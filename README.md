1)
Вычисление diff базируется на событиях доктрины.
Посколько объект события PreUpdate содержит измененения по полям, то данные по измененнным полям берем оттуда.
Изменения по связанным объектам приходится высчитывать, обращаясь к коллекции которая хранит связанные с сущностью объекты.

Логика хранения изменений была вынесена в отдельных компонент. Преимуществом этого подхода является возможность переиспользования этого компонента в других контроллерах с другими сущностями.
Плюс там можно поместить логику хранения этих изменений. Например производить запись в БД.  

2) 
Есть таблица books со структурой: 
book_id, title, description, author_id, status, date (timestamp)

Таблица authors: 

author_id, name


Предположим, что у нас в базе хранятся 1 000 000 книг и нам необходимо сделать вывод списка книг с авторами и постраничной навигацией, книги со статусом (status) равным двум и отсортированным по дате (date). 
Напишите, как бы Вы это сделали и какие индексы (если нужно) Вы бы добавили к таблицам и почему? 

Действия по оптимизации:

- Не использовать join books с authors, так как на каждую строку из books будет производится поиск в authors, и на большом кол-во строк будет заметно ухудшение производительности. Лучше всего сначала выбрать все книги и только потом сделать запрос на выборку авторов, заранее собрав все autohor_id из выборки книг.
- create index (status, date). Status - для where условия. date - для сортировки.
- partition by range ( YEAR (date) ) // Тут надо анализировать имеющиеся данные в таблице. Возможно оптимальнее будет партиционировать по MONTH(date)
