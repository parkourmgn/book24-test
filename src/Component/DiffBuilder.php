<?php

namespace App\Component;

use App\Entity\EntityInterface;

class DiffBuilder
{
    /**
     * @var EntityInterface
     */
    private $entity;

    private $fields = [];

    private $associations = [];

    private function __construct(EntityInterface $entity)
    {
        $this->entity = $entity;
    }

    public static function create(EntityInterface $entity): DiffBuilder
    {
        return new self($entity);
    }

    public static function noDiff(EntityInterface $entity): Diff
    {
        return (new self($entity))->build();
    }

    public function build(): Diff
    {
        return Diff::createFromBuilder($this);
    }

    /**
     * @return EntityInterface
     */
    public function getEntity(): EntityInterface
    {
        return $this->entity;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @return array
     */
    public function getAssociations(): array
    {
        return $this->associations;
    }

    public function addField(string $name, $oldValue, $newValue): DiffBuilder
    {
        $this->fields[$name] = [
            $oldValue, $newValue
        ];

        return $this;
    }

    public function addAssociation(string $name, array $inserted, array $deleted): DiffBuilder
    {
        $this->associations[$name] = [
            $inserted, $deleted
        ];

        return $this;
    }
}