<?php

namespace App\Component;

use App\Entity\EntityInterface;

class DiffStorage
{
    /**
     * @var Diff[]
     */
    private $diffs = [];

    public function __construct()
    {
        $this->diffs = new \SplObjectStorage();
    }

    public function store(Diff $diff): void
    {
        $this->diffs[$diff->getEntity()] = $diff;
    }

    public function getDiff(EntityInterface $entity): Diff
    {
        return $this->diffs[$entity] ?? DiffBuilder::noDiff($entity);
    }
}