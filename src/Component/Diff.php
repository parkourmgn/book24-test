<?php

namespace App\Component;

use App\Entity\EntityInterface;

class Diff
{
    /**
     * @var EntityInterface
     */
    private $entity;

    /**
     * @var array
     */
    private $fields;

    /**
     * @var array
     */
    private $associations;

    /**
     * Diff constructor.
     * @param EntityInterface $entity
     * @param array $fields
     * @param array $associations
     */
    public function __construct(EntityInterface $entity, array $fields, array $associations)
    {
        $this->entity = $entity;
        $this->fields = $fields;
        $this->associations = $associations;
    }

    /**
     * @return EntityInterface
     */
    public function getEntity(): EntityInterface
    {
        return $this->entity;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @return array
     */
    public function getAssociations(): array
    {
        return $this->associations;
    }

    public static function createFromBuilder(DiffBuilder $builder): Diff
    {
        return new self($builder->getEntity(), $builder->getFields(), $builder->getAssociations());
    }

    public function toArray(): array
    {
        $data = [
            'entity' => (string) $this->entity,
            'fields' => $this->fields,
            'associations' => [],
        ];

        foreach ($this->associations as $association => [$inserted, $deleted]) {
            $inserted = array_map('strval', $inserted);
            $deleted = array_map('strval', $deleted);

            $data['associations'][$association] = [
                $inserted, $deleted
            ];
        }

        return $data;
    }
}