<?php

namespace App\EntityListener;

use App\Component\DiffBuilder;
use App\Component\DiffStorage;
use App\Entity\Product;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;

class ProductEntityListener
{
    /**
     * @var DiffStorage
     */
    private $diffStorage;

    /**
     * ProductEntityListener constructor.
     * @param DiffStorage $diffStorage
     */
    public function __construct(DiffStorage $diffStorage)
    {
        $this->diffStorage = $diffStorage;
    }

    /**
     * @param Product $product
     * @param PreUpdateEventArgs $eventArgs
     * @ORM\PreUpdate()
     */
    public function preUpdateHandler(Product $product, PreUpdateEventArgs $eventArgs): void
    {
        $diffBuilder = DiffBuilder::create($product);

        foreach ($eventArgs->getEntityChangeSet() as $field => [$oldValue, $newValue]) {
            $diffBuilder->addField($field, $oldValue, $newValue);
        }

        if ($product->getSomethings()->isDirty()) {
            $diffBuilder->addAssociation(
                'somethings',
                $product->getSomethings()->getInsertDiff(),
                $product->getSomethings()->getDeleteDiff()
            );
        }

        $this->diffStorage->store($diffBuilder->build());
    }
}