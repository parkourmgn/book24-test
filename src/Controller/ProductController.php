<?php

namespace App\Controller;

use App\Component\DiffStorage;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends AbstractController
{
    private const PRODUCT_ID = 1;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var DiffStorage
     */
    private $diffStorage;

    /**
     * @var EntityRepository
     */
    private $productRepository;

    /**
     * ProductController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager, DiffStorage $diffStorage)
    {
        $this->entityManager = $entityManager;
        $this->diffStorage = $diffStorage;

        $this->productRepository = $this->entityManager->getRepository(Product::class);
    }

    /**
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function index(): Response
    {
        $product = $this->getProduct();

        $product->setNomCode('Updated code ' . bin2hex(random_bytes(10)));
        $product->setName('Updated name ' . bin2hex(random_bytes(10)));
        $product->addSomething();

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        $diff = $this->diffStorage->getDiff($product);

        return new JsonResponse($diff->toArray());
    }

    /**
     * @return Product
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function getProduct(): Product
    {
        $product = $this->productRepository->find(self::PRODUCT_ID);

        if ($product === null) {
            $product = new Product(self::PRODUCT_ID, 'new code', 'new product');

            $this->entityManager->persist($product);
            $this->entityManager->flush();
        }

        return $product;
    }
}