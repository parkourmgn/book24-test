<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity
 * @ORM\EntityListeners({"App\EntityListener\ProductEntityListener"})
 */
class Product implements EntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private $productId;

    /**
     * @ORM\Column(type="string")
     */
    private $nomCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $isNew;

    /**
     * @var PersistentCollection | ArrayCollection
     * @ORM\OneToMany(targetEntity="ProductSomething", mappedBy="product", cascade={"all"})
     */
    private $somethings;

    /**
     * Product constructor.
     * @param $productId
     * @param $nomCode
     * @param $name
     */
    public function __construct(int $productId, string $nomCode, string $name)
    {
        $this->productId = $productId;
        $this->nomCode = $nomCode;
        $this->name = $name;

        $this->isNew = true;

        $this->somethings = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->productId;
    }

    /**
     * @return string
     */
    public function getNomCode(): string
    {
        return $this->nomCode;
    }

    /**
     * @param string $nomCode
     */
    public function setNomCode(string $nomCode): void
    {
        $this->nomCode = $nomCode;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist(): void
    {
        $this->isNew = false;
    }

    public function addSomething(): void
    {
        $this->somethings->add(new ProductSomething($this));
    }

    /**
     * @return PersistentCollection
     */
    public function getSomethings(): PersistentCollection
    {
        return $this->somethings;
    }

    public function __toString()
    {
        return 'Product#' . $this->getId();
    }
}
