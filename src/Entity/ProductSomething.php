<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ProductSomething
 * @package App\Entity
 * @ORM\Entity()
 */
class ProductSomething
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Product
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="somethings")
     * @ORM\JoinColumn(name="productId", referencedColumnName="product_id")
     */
    private $product;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function __construct(Product $product)
    {
        $this->product = $product;
        $this->createdAt = new \DateTime();
    }


    public function __toString()
    {
        return 'ProductSomething#' . $this->id;
    }
}